<h2>Formulaire voiture</h2>
    <form action="form.php" method="get">
        <div>
            <label>De quelle marque est votre voiture ?</label>
            <input name="marque" id="marque"/>
        </div>
        <div>
            <label>Quel est le modèle ?</label>
            <input name="modele" id="modele"/>
        </div>
        <div>
            <label>Quel est son immatriculation ?</label>
            <input name="immat" id="immat"/>
        </div>
        <br>
        <div>
            <button>Voir les voitures disponibles</button>
        </div>
    </form>