<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';

$title = "Bienvenue sur notre magnifique site de location automobile !";
$content = "Vous pouvez ici louer la voiture de vos rêves, pour cela vous avez juste à vous inscrire puis à sélectionner vos critères. Simple et efficace !";

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Projet Cauvet Boyer</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	    <?php include_once 'inc/header.php';

            require('inc/page.php');

        include_once 'inc/footer.php'; ?>
    </body>
</html>