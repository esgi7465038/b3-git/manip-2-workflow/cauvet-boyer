<?php
/**
 * ATTENTION!! 
 * Les deux lignes PHP suivantes doivent être incluses dans toutes vos pages "exécutable"
 */

//  Permet d'utiliser le typage fort si strict_types=1
//  ATTENTION!! Laisser en première ligne de toutes vos pages
declare(strict_types=1);

require_once '../config/appConfig.php';

$title = "Bienvenue la page de recherche de véhicule";
$content = "Selon vos critères, on vous propose les meilleurs véhicules au meilleur prix !";
$form = true;

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Projet Cauvet Boyer</title>
	<?php include_once 'inc/head.php'; ?>
    </head>
    <body>
	    <?php include_once 'inc/header.php';

            require('inc/page.php');

        include_once 'inc/footer.php'; ?>
    </body>
</html>
