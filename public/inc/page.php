<main>
	<div id="bandeau">
	<button class="boutonBandeau" onclick="window.location.href = 'homepage.php'">Accueil</button>
		<button class="boutonBandeau" onclick="window.location.href = 'inscription.php'">S'inscrire</button>
		<button class="boutonBandeau" onclick="window.location.href = 'rechercheVoiture.php'">Louer une voiture</button>
	</div>
	<article>
		<header>
		    <h1><?=$title ?></h1>
		</header>
		<p><?=$content ?></p>
        <?php if (isset($form)) {
            include_once 'form.php';
        } ?>
	</article>
</main>