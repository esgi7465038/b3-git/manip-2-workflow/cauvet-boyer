<?php 
class Voiture
{
    // constructor
    public function __construct (public readonly string $marque, public readonly string $modele, public readonly string $immatriculation)
    {
        $this->marque = $marque;
        $this->modele = $modele;
        $this->immatriculation = $immatriculation;
    }

    // getters 
    public function getVoiture()
    {
        return $this->marque;
        return $this->modele;
        return $this->immatriculation;
    }
}